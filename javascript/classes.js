var Piece = function(data) {
    this.length = data.length || 0;
    this.switch = data.switch || false;
    this.radius = data.radius || 0;
    this.angle = data.angle || 0;
};

var Lane = function(data) {
    this.distanceFromCenter = data.distanceFromCenter || 0;
    this.index = data.index || -1;
};

module.exports.Piece = Piece;
module.exports.Lane = Lane;