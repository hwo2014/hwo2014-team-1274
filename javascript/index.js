var classes = require("./classes");
var Car = require("./car");
var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var pieces = [];
var car;
var carColor;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    if (data.msgType === 'carPositions') {
        car.updatePiece(data.data[0].piecePosition.pieceIndex, data.data[0].piecePosition.inPieceDistance);
        car.updateAngle(data.data[0].angle);
        car.tick(data.gameTick);
        console.log("s: " + car.speed + " t: " + car.throttle + " a: " + car.angle + " c: " + car.curve + " ac: " + car.aceleration);
        console.log("c: " + car.curve);
        send({
          msgType: "throttle",
          data: car.getThrottle()
        });
    } 
    else if (data.msgType === 'join') {
        console.log('Joined');
    } else if (data.msgType === 'gameStart') {
        console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
        console.log('Race ended');
    } else if (data.msgType === 'yourCar') {
        console.log("Your car ", data.data.name, " color ", data.data.color );
        carColor = data.data.color;
        console.log(carColor);
    } else if (data.msgType === 'gameInit') {
        var pieces2 = data.data.race.track.pieces;
        for (var p = 0; p < pieces2.length; p++) {
            var piece = new classes.Piece(pieces2[p]);
            pieces.push(piece);
        }
        for (var i = 0; i < data.data.race.cars.length; i++) {
            if (data.data.race.cars[i].id.color === carColor) {
                car = new Car(data.data.race.cars[i], pieces);
                console.log(JSON.stringify(car));
            }
            console.log(data.data.race.cars[i].id.color);
        }
        console.log(JSON.stringify(pieces));
    }
    else {
        send({
          msgType: "ping",
          data: {}
        });
    }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
