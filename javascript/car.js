var ua = require('universal-analytics');
var visitor = ua('UA-43898944-1');
var eventParams = {
  ec: "HWO",
  ea: "Tick",
  el: "speed",
  ev: 42
};

var Car = function(data, pieces) {
    this.name = data.id.name || "";
    this.color = data.id.color || "invisible";
    this.length = data.dimensions.length || 0;
    this.width = data.dimensions.width || 0;
    this.guideFlag = data.dimensions.guideFlagPosition || 0;
    this.angle = 0;
    this.pieces = pieces;
    this.lastPieceIndex = 0;
    this.pieceIndex = 0;
    this.lastPosInPiece = 0;
    this.posInPiece = 0;
    this.priorAngle = 0;
    this.priorThrottle = 0.5;
    this.throttle = 0.5;
    this.speed = 0;
    this.ticks = 0;
    this.curve = 0;
    this.aceleration = 0;
    this.lastCurve = 0;
    this.rCurve = 0;
};

Car.prototype = {
    updateCurve : function() {
        var nextPiece2 = this.getNextPiece(2) || {angle: 0};
        if (this.getCurPiece().angle >= 45 && this.curve == 180) {
            this.lastCurve = 180;
        }
        else if (this.getCurPiece().angle <= 45 && this.curve == 180) {
            this.lastCurve = -180;
        }
        else {
            this.lastCurve = 0;
        }
        if (Math.abs(this.getNextPiece().angle) >= 45 && Math.abs(this.getNextPiece(2).angle) >= 45 && Math.abs(this.getNextPiece(3).angle) >= 45 && Math.abs(this.getNextPiece(4).angle) >= 45) {
            this.curve = 180;
        }
        else if (Math.abs(nextPiece2.angle) === 0) {
            this.curve = 0;
        }
        if (this.getNextPiece().angle >= 45) {
            this.rCurve = 180;
        }
        else if (this.getNextPiece().angle >= 45) {
            this.rCurve = -180;
        }
        else if (this.curve === 0) {
            this.rCurve = 0;
        }
    },
    tick : function (gameTicks) {
        this.updateCurve();
        this.updateThrottle();
        var vDist;
        var vTick = gameTicks - this.ticks;
        if (this.lastPieceIndex == this.pieceIndex) {
            vDist = this.posInPiece - this.lastPosInPiece;
        }
        else {
            var lastPieceDist;
            if (this.pieces[this.lastPieceIndex].length === 0) {
                lastPieceDist = this.pieces[this.lastPieceIndex].radius - this.lastPosInPiece;
            }
            else {
                lastPieceDist = this.pieces[this.lastPieceIndex].length - this.lastPosInPiece;
            }
            vDist = lastPieceDist + this.posInPiece;
        }
        
        var vSpeed = this.speed;
        this.speed = vDist / vTick;
        vSpeed = this.speed - vSpeed;
        this.aceleration = vSpeed / vTick;
/*        eventParams.el = "speed";
        eventParams.ev = this.speed;
        visitor.event(eventParams).send();
        eventParams.el = "throttle";
        eventParams.ev = this.throttle;
        visitor.event(eventParams).send();
        eventParams.el = "angle";
        eventParams.ev = this.angle;
        visitor.event(eventParams).send();*/
        this.ticks = gameTicks;
    },
    updatePiece : function (pieceIndex, piecePos) {
        this.lastPieceIndex = this.pieceIndex;
        this.pieceIndex = pieceIndex;
        this.lastPosInPiece = this.posInPiece;
        this.posInPiece = piecePos;
    },
    getCurPiece : function () {
        return this.pieces[(this.pieceIndex)];
    },
    getNextPiece : function (inc) {
        inc = inc || 1;
        var rPiece;
        if (this.pieces.length <= this.pieceIndex + inc) {
            rPiece = this.pieces[this.pieces.length - (this.pieceIndex + inc)];
        }
        else {
            rPiece = this.pieces[(this.pieceIndex + inc)];
        }
        if(typeof rPiece == "undefined") {
            rPiece = this.pieces[0];
        }
        return rPiece;
    },
    updateAngle : function (angle) {
        this.priorAngle = this.angle;
        this.angle = angle;    
    },
    getDifAngle : function() {
        return this.priorAngle - this.angle;
    },
    getAngle : function() {
        return this.angle;
    },
    getThrottle : function () {
        return this.throttle;
    },
    updateThrottle : function() {
        console.log("NextPiece angle " + this.getNextPiece().angle);
        if (this.priorThrottle === 0.0) {
            this.priorThrottle = 0.1;
            return;
        }
        this.priorThrottle = this.throttle;
        if ((this.rCurve == 180 && this.lastCurve == -180) || (this.rCurve == -180 && this.lastCurve == 180)) {
            this.throttle = 1.0;
        }
        else if (Math.abs(this.getNextPiece(2).angle) >= 45 && Math.abs(this.getNextPiece(3).angle) >= 45 && Math.abs(this.getNextPiece(4).angle) >= 45 && Math.abs(this.getNextPiece(5).angle) >= 45
        && this.speed > 6) {
            this.throttle = 0.1;
        }
        else if (this.curve == 180 && this.speed > 7.0) {
            console.log("T2");
            this.throttle = 0.1;
        }
        else if ((this.curve != 180 && this.getNextPiece().angle === 0) || (this.curve != 180 && ((this.getNextPiece().angle === 0 || Math.abs(this.getNextPiece().angle) < 30) && Math.abs(this.angle) > 40))) {
            console.log("T1");
            this.throttle = 1.0;
        }
        else if (this.curve == 180 && Math.abs(this.angle) <= 5) {
            console.log("T9");
            this.throttle = 0.7;
        }
        else if (this.curve == 180 && Math.abs(this.getDifAngle()) >= 2.3) {
            console.log("T3");
            this.throttle = 0.1;
        }
        else if (this.curve == 180 && Math.abs(this.getDifAngle()) >= 1.2 && this.speed > 6) {
            console.log("T4");
            this.throttle = 0.2;
        }
        else if (this.curve == 180 && Math.abs(this.angle) > 36 && Math.abs(this.angle) < 43) {
            console.log("T5");
            this.throttle = 0.55;
        }
        else if (this.curve == 180 && Math.abs(this.angle) > 43) {
            console.log("T6");
            this.throttle = 0.45;
        }
        else if (this.getNextPiece().angle !== 0) {
            console.log("T7");
            this.throttle = 0.6;
        }
        else {
            console.log("T8");
            this.throttle = 0.9;
        }
        /*if ((this.angle > 5 && this.angle < 10) || (this.angle < -5 && this.angle > -10)) {
            return 0.7;
        }
        else if ((this.angle > 10 && this.angle < 15) || (this.angle < -10 && this.angle > -15)) {
            return 0.7;
        }
        else if ((this.angle > 15 && this.angle < 20) || (this.angle < -15 && this.angle > -20)) {
            return 0.6;
        }
        else if ((this.angle > 20 && this.angle < 25) || (this.angle < -20 && this.angle > -25)) {
            return 0.5;
        }
        else if ((this.angle > 25 && this.angle < 30) || (this.angle < -25 && this.angle > -30)) {
            return 0.4;
        }
        else if ((this.angle > 30 && this.angle < 35) || (this.angle < -30 && this.angle > -35)) {
            return 0.3;
        }
        else if ((this.angle > 35 && this.angle < 40) || (this.angle < -35 && this.angle > -40)) {
            return 0.2;
        }
        else if ((this.angle > 40 && this.angle < 45) || (this.angle < -40 && this.angle > -45)) {
            return 0.1;
        }
        else if ((this.angle > 45 && this.angle < 50) || (this.angle < -45 && this.angle > -50)) {
            return 0.0;
        }
        else if (this.angle > 50 || this.angle < -50) {
            return 0.0;
        }
        else {
            return 1.0;
        }*/
    }    
};

module.exports = Car;